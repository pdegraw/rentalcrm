# == Schema Information
#
# Table name: jobs
#
#  id                   :integer          not null, primary key
#  user_id              :integer
#  contact_id           :integer
#  title                :string(64)       not null
#  job_number           :string(12)
#  invoice_number       :string(12)
#  invoiced             :string(12)
#  stage                :string(23)       default("Inquiry")
#  starts_at            :time
#  ends_at              :time
#  confirmed_load_times :string(12)
#  created_at           :datetime         not null
#  updated_at           :datetime         not null
#  loads_in_at          :datetime
#  loads_out_at         :datetime
#  starts_on            :date
#  ends_on              :date
#  loads_in_on          :date
#  loads_out_on         :date
#

FactoryGirl.define do
  factory :job do
    user nil
    contact nil
    title "MyString"
    job_number "MyString"
    invoice_number "MyString"
    invoiced "MyString"
    stage "MyString"
    starts_at "2016-03-03 22:53:54"
    ends_at "2016-03-03 22:53:54"
    loads_in_at "2016-03-03 22:53:54"
    loads_out_at "2016-03-03 22:53:54"
    confirmed_load_times "MyString"
  end
end
