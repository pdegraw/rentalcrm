# == Schema Information
#
# Table name: contacts
#
#  id         :integer          not null, primary key
#  user_id    :integer
#  first_name :string(64)
#  last_name  :string(64)
#  title      :string(64)
#  department :string(64)
#  source     :string(32)
#  email      :string(64)
#  alt_email  :string(64)
#  phone      :string(32)
#  mobile     :string(32)
#  fax        :string(32)
#  created_at :datetime         not null
#  updated_at :datetime         not null
#  account_id :integer
#

FactoryGirl.define do
  factory :contact do
    user nil
    assigned_to 1
    first_name "MyString"
    last_name "MyString"
    title "MyString"
    department "MyString"
    source "MyString"
    email "MyString"
    alt_email "MyString"
    phone "MyString"
    mobile "MyString"
    fax "MyString"
  end
end
