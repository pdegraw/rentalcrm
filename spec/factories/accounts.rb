# == Schema Information
#
# Table name: accounts
#
#  id         :integer          not null, primary key
#  user_id    :integer
#  name       :string(64)       not null
#  website    :string(64)
#  phone      :string(32)       not null
#  fax        :string(32)
#  email      :string(32)
#  created_at :datetime         not null
#  updated_at :datetime         not null
#  contact_id :integer
#

FactoryGirl.define do
  factory :account do
    user nil
    name "MyString"
    website "MyString"
    phone "MyString"
    fax "MyString"
    email "MyString"
  end
end
