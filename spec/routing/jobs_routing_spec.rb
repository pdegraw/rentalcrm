# == Schema Information
#
# Table name: jobs
#
#  id                   :integer          not null, primary key
#  user_id              :integer
#  contact_id           :integer
#  title                :string(64)       not null
#  job_number           :string(12)
#  invoice_number       :string(12)
#  invoiced             :string(12)
#  stage                :string(23)       default("Inquiry")
#  starts_at            :time
#  ends_at              :time
#  confirmed_load_times :string(12)
#  created_at           :datetime         not null
#  updated_at           :datetime         not null
#  loads_in_at          :datetime
#  loads_out_at         :datetime
#  starts_on            :date
#  ends_on              :date
#  loads_in_on          :date
#  loads_out_on         :date
#

require "rails_helper"

RSpec.describe JobsController, type: :routing do
  describe "routing" do

    it "routes to #index" do
      expect(:get => "/jobs").to route_to("jobs#index")
    end

    it "routes to #new" do
      expect(:get => "/jobs/new").to route_to("jobs#new")
    end

    it "routes to #show" do
      expect(:get => "/jobs/1").to route_to("jobs#show", :id => "1")
    end

    it "routes to #edit" do
      expect(:get => "/jobs/1/edit").to route_to("jobs#edit", :id => "1")
    end

    it "routes to #create" do
      expect(:post => "/jobs").to route_to("jobs#create")
    end

    it "routes to #update via PUT" do
      expect(:put => "/jobs/1").to route_to("jobs#update", :id => "1")
    end

    it "routes to #update via PATCH" do
      expect(:patch => "/jobs/1").to route_to("jobs#update", :id => "1")
    end

    it "routes to #destroy" do
      expect(:delete => "/jobs/1").to route_to("jobs#destroy", :id => "1")
    end

  end
end
