# == Schema Information
#
# Table name: contacts
#
#  id         :integer          not null, primary key
#  user_id    :integer
#  first_name :string(64)
#  last_name  :string(64)
#  title      :string(64)
#  department :string(64)
#  source     :string(32)
#  email      :string(64)
#  alt_email  :string(64)
#  phone      :string(32)
#  mobile     :string(32)
#  fax        :string(32)
#  created_at :datetime         not null
#  updated_at :datetime         not null
#  account_id :integer
#

require "rails_helper"

RSpec.describe ContactsController, type: :routing do
  describe "routing" do

    it "routes to #index" do
      expect(:get => "/contacts").to route_to("contacts#index")
    end

    it "routes to #new" do
      expect(:get => "/contacts/new").to route_to("contacts#new")
    end

    it "routes to #show" do
      expect(:get => "/contacts/1").to route_to("contacts#show", :id => "1")
    end

    it "routes to #edit" do
      expect(:get => "/contacts/1/edit").to route_to("contacts#edit", :id => "1")
    end

    it "routes to #create" do
      expect(:post => "/contacts").to route_to("contacts#create")
    end

    it "routes to #update via PUT" do
      expect(:put => "/contacts/1").to route_to("contacts#update", :id => "1")
    end

    it "routes to #update via PATCH" do
      expect(:patch => "/contacts/1").to route_to("contacts#update", :id => "1")
    end

    it "routes to #destroy" do
      expect(:delete => "/contacts/1").to route_to("contacts#destroy", :id => "1")
    end

  end
end
