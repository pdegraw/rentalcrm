require 'rails_helper'

RSpec.describe "jobs/index", type: :view do
  before(:each) do
    assign(:jobs, [
      Job.create!(
        :user => nil,
        :contact => nil,
        :title => "Title",
        :job_number => "Job Number",
        :invoice_number => "Invoice Number",
        :invoiced => "Invoiced",
        :stage => "Stage",
        :confirmed_load_times => "Confirmed Load Times"
      ),
      Job.create!(
        :user => nil,
        :contact => nil,
        :title => "Title",
        :job_number => "Job Number",
        :invoice_number => "Invoice Number",
        :invoiced => "Invoiced",
        :stage => "Stage",
        :confirmed_load_times => "Confirmed Load Times"
      )
    ])
  end

  it "renders a list of jobs" do
    render
    assert_select "tr>td", :text => nil.to_s, :count => 2
    assert_select "tr>td", :text => nil.to_s, :count => 2
    assert_select "tr>td", :text => "Title".to_s, :count => 2
    assert_select "tr>td", :text => "Job Number".to_s, :count => 2
    assert_select "tr>td", :text => "Invoice Number".to_s, :count => 2
    assert_select "tr>td", :text => "Invoiced".to_s, :count => 2
    assert_select "tr>td", :text => "Stage".to_s, :count => 2
    assert_select "tr>td", :text => "Confirmed Load Times".to_s, :count => 2
  end
end
