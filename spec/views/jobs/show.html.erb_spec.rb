require 'rails_helper'

RSpec.describe "jobs/show", type: :view do
  before(:each) do
    @job = assign(:job, Job.create!(
      :user => nil,
      :contact => nil,
      :title => "Title",
      :job_number => "Job Number",
      :invoice_number => "Invoice Number",
      :invoiced => "Invoiced",
      :stage => "Stage",
      :confirmed_load_times => "Confirmed Load Times"
    ))
  end

  it "renders attributes in <p>" do
    render
    expect(rendered).to match(//)
    expect(rendered).to match(//)
    expect(rendered).to match(/Title/)
    expect(rendered).to match(/Job Number/)
    expect(rendered).to match(/Invoice Number/)
    expect(rendered).to match(/Invoiced/)
    expect(rendered).to match(/Stage/)
    expect(rendered).to match(/Confirmed Load Times/)
  end
end
