require 'rails_helper'

RSpec.describe "jobs/edit", type: :view do
  before(:each) do
    @job = assign(:job, Job.create!(
      :user => nil,
      :contact => nil,
      :title => "MyString",
      :job_number => "MyString",
      :invoice_number => "MyString",
      :invoiced => "MyString",
      :stage => "MyString",
      :confirmed_load_times => "MyString"
    ))
  end

  it "renders the edit job form" do
    render

    assert_select "form[action=?][method=?]", job_path(@job), "post" do

      assert_select "input#job_user_id[name=?]", "job[user_id]"

      assert_select "input#job_contact_id[name=?]", "job[contact_id]"

      assert_select "input#job_title[name=?]", "job[title]"

      assert_select "input#job_job_number[name=?]", "job[job_number]"

      assert_select "input#job_invoice_number[name=?]", "job[invoice_number]"

      assert_select "input#job_invoiced[name=?]", "job[invoiced]"

      assert_select "input#job_stage[name=?]", "job[stage]"

      assert_select "input#job_confirmed_load_times[name=?]", "job[confirmed_load_times]"
    end
  end
end
