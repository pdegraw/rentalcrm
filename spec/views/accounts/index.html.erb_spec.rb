require 'rails_helper'

RSpec.describe "accounts/index", type: :view do
  before(:each) do
    assign(:accounts, [
      Account.create!(
        :user => nil,
        :name => "Name",
        :website => "Website",
        :phone => "Phone",
        :fax => "Fax",
        :email => "Email"
      ),
      Account.create!(
        :user => nil,
        :name => "Name",
        :website => "Website",
        :phone => "Phone",
        :fax => "Fax",
        :email => "Email"
      )
    ])
  end

  it "renders a list of accounts" do
    render
    assert_select "tr>td", :text => nil.to_s, :count => 2
    assert_select "tr>td", :text => "Name".to_s, :count => 2
    assert_select "tr>td", :text => "Website".to_s, :count => 2
    assert_select "tr>td", :text => "Phone".to_s, :count => 2
    assert_select "tr>td", :text => "Fax".to_s, :count => 2
    assert_select "tr>td", :text => "Email".to_s, :count => 2
  end
end
