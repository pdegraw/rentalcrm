require 'rails_helper'

RSpec.describe "accounts/edit", type: :view do
  before(:each) do
    @account = assign(:account, Account.create!(
      :user => nil,
      :name => "MyString",
      :website => "MyString",
      :phone => "MyString",
      :fax => "MyString",
      :email => "MyString"
    ))
  end

  it "renders the edit account form" do
    render

    assert_select "form[action=?][method=?]", account_path(@account), "post" do

      assert_select "input#account_user_id[name=?]", "account[user_id]"

      assert_select "input#account_name[name=?]", "account[name]"

      assert_select "input#account_website[name=?]", "account[website]"

      assert_select "input#account_phone[name=?]", "account[phone]"

      assert_select "input#account_fax[name=?]", "account[fax]"

      assert_select "input#account_email[name=?]", "account[email]"
    end
  end
end
