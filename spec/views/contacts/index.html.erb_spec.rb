require 'rails_helper'

RSpec.describe "contacts/index", type: :view do
  before(:each) do
    assign(:contacts, [
      Contact.create!(
        :user => nil,
        :assigned_to => 1,
        :first_name => "First Name",
        :last_name => "Last Name",
        :title => "Title",
        :department => "Department",
        :source => "Source",
        :email => "Email",
        :alt_email => "Alt Email",
        :phone => "Phone",
        :mobile => "Mobile",
        :fax => "Fax"
      ),
      Contact.create!(
        :user => nil,
        :assigned_to => 1,
        :first_name => "First Name",
        :last_name => "Last Name",
        :title => "Title",
        :department => "Department",
        :source => "Source",
        :email => "Email",
        :alt_email => "Alt Email",
        :phone => "Phone",
        :mobile => "Mobile",
        :fax => "Fax"
      )
    ])
  end

  it "renders a list of contacts" do
    render
    assert_select "tr>td", :text => nil.to_s, :count => 2
    assert_select "tr>td", :text => 1.to_s, :count => 2
    assert_select "tr>td", :text => "First Name".to_s, :count => 2
    assert_select "tr>td", :text => "Last Name".to_s, :count => 2
    assert_select "tr>td", :text => "Title".to_s, :count => 2
    assert_select "tr>td", :text => "Department".to_s, :count => 2
    assert_select "tr>td", :text => "Source".to_s, :count => 2
    assert_select "tr>td", :text => "Email".to_s, :count => 2
    assert_select "tr>td", :text => "Alt Email".to_s, :count => 2
    assert_select "tr>td", :text => "Phone".to_s, :count => 2
    assert_select "tr>td", :text => "Mobile".to_s, :count => 2
    assert_select "tr>td", :text => "Fax".to_s, :count => 2
  end
end
