require 'rails_helper'

RSpec.describe "contacts/edit", type: :view do
  before(:each) do
    @contact = assign(:contact, Contact.create!(
      :user => nil,
      :assigned_to => 1,
      :first_name => "MyString",
      :last_name => "MyString",
      :title => "MyString",
      :department => "MyString",
      :source => "MyString",
      :email => "MyString",
      :alt_email => "MyString",
      :phone => "MyString",
      :mobile => "MyString",
      :fax => "MyString"
    ))
  end

  it "renders the edit contact form" do
    render

    assert_select "form[action=?][method=?]", contact_path(@contact), "post" do

      assert_select "input#contact_user_id[name=?]", "contact[user_id]"

      assert_select "input#contact_assigned_to[name=?]", "contact[assigned_to]"

      assert_select "input#contact_first_name[name=?]", "contact[first_name]"

      assert_select "input#contact_last_name[name=?]", "contact[last_name]"

      assert_select "input#contact_title[name=?]", "contact[title]"

      assert_select "input#contact_department[name=?]", "contact[department]"

      assert_select "input#contact_source[name=?]", "contact[source]"

      assert_select "input#contact_email[name=?]", "contact[email]"

      assert_select "input#contact_alt_email[name=?]", "contact[alt_email]"

      assert_select "input#contact_phone[name=?]", "contact[phone]"

      assert_select "input#contact_mobile[name=?]", "contact[mobile]"

      assert_select "input#contact_fax[name=?]", "contact[fax]"
    end
  end
end
