require 'rails_helper'

RSpec.describe "contacts/show", type: :view do
  before(:each) do
    @contact = assign(:contact, Contact.create!(
      :user => nil,
      :assigned_to => 1,
      :first_name => "First Name",
      :last_name => "Last Name",
      :title => "Title",
      :department => "Department",
      :source => "Source",
      :email => "Email",
      :alt_email => "Alt Email",
      :phone => "Phone",
      :mobile => "Mobile",
      :fax => "Fax"
    ))
  end

  it "renders attributes in <p>" do
    render
    expect(rendered).to match(//)
    expect(rendered).to match(/1/)
    expect(rendered).to match(/First Name/)
    expect(rendered).to match(/Last Name/)
    expect(rendered).to match(/Title/)
    expect(rendered).to match(/Department/)
    expect(rendered).to match(/Source/)
    expect(rendered).to match(/Email/)
    expect(rendered).to match(/Alt Email/)
    expect(rendered).to match(/Phone/)
    expect(rendered).to match(/Mobile/)
    expect(rendered).to match(/Fax/)
  end
end
