# == Schema Information
#
# Table name: contacts
#
#  id         :integer          not null, primary key
#  user_id    :integer
#  first_name :string(64)
#  last_name  :string(64)
#  title      :string(64)
#  department :string(64)
#  source     :string(32)
#  email      :string(64)
#  alt_email  :string(64)
#  phone      :string(32)
#  mobile     :string(32)
#  fax        :string(32)
#  created_at :datetime         not null
#  updated_at :datetime         not null
#  account_id :integer
#

require 'rails_helper'

RSpec.describe "Contacts", type: :request do
  describe "GET /contacts" do
    it "works! (now write some real specs)" do
      get contacts_path
      expect(response).to have_http_status(200)
    end
  end
end
