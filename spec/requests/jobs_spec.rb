# == Schema Information
#
# Table name: jobs
#
#  id                   :integer          not null, primary key
#  user_id              :integer
#  contact_id           :integer
#  title                :string(64)       not null
#  job_number           :string(12)
#  invoice_number       :string(12)
#  invoiced             :string(12)
#  stage                :string(23)       default("Inquiry")
#  starts_at            :time
#  ends_at              :time
#  confirmed_load_times :string(12)
#  created_at           :datetime         not null
#  updated_at           :datetime         not null
#  loads_in_at          :datetime
#  loads_out_at         :datetime
#  starts_on            :date
#  ends_on              :date
#  loads_in_on          :date
#  loads_out_on         :date
#

require 'rails_helper'

RSpec.describe "Jobs", type: :request do
  describe "GET /jobs" do
    it "works! (now write some real specs)" do
      get jobs_path
      expect(response).to have_http_status(200)
    end
  end
end
