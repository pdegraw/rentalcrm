# == Schema Information
#
# Table name: accounts
#
#  id         :integer          not null, primary key
#  user_id    :integer
#  name       :string(64)       not null
#  website    :string(64)
#  phone      :string(32)       not null
#  fax        :string(32)
#  email      :string(32)
#  created_at :datetime         not null
#  updated_at :datetime         not null
#  contact_id :integer
#

require 'rails_helper'

RSpec.describe "Accounts", type: :request do
  describe "GET /accounts" do
    it "works! (now write some real specs)" do
      get accounts_path
      expect(response).to have_http_status(200)
    end
  end
end
