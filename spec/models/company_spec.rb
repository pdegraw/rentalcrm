# == Schema Information
#
# Table name: companies
#
#  id         :integer          not null, primary key
#  name       :string(64)
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

require 'rails_helper'

RSpec.describe Company, type: :model do
  context "Model:" do

		it "name should be present" do
		    should validate_presence_of(:name)
	    end
		
		it "should have one user" do
# 			should validate_presence_of(:role).
# 			on(:create)
		end

	end

	context "ActiveRecords:" do

		it "should have many users" do
			should have_many(:users)
		end

	end
end
