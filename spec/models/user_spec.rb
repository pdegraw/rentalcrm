# == Schema Information
#
# Table name: users
#
#  id                     :integer          not null, primary key
#  email                  :string           default(""), not null
#  encrypted_password     :string           default(""), not null
#  reset_password_token   :string
#  reset_password_sent_at :datetime
#  remember_created_at    :datetime
#  sign_in_count          :integer          default(0), not null
#  current_sign_in_at     :datetime
#  last_sign_in_at        :datetime
#  current_sign_in_ip     :inet
#  last_sign_in_ip        :inet
#  created_at             :datetime         not null
#  updated_at             :datetime         not null
#  role                   :integer
#  company_id             :integer
#  first_name             :string(64)
#  last_name              :string(64)
#

require 'rails_helper'

RSpec.describe User, type: :model do
  context "Model:" do

		context "email should be present" do
			it { should validate_presence_of(:email) }
	    end

		context "email should be unique" do
			it { should validate_uniqueness_of(:email) }
		end
		
		context "company should be present" do
		    it { should validate_presence_of(:company) }
	    end
		
		context "should set a role to User" do
			it { should validate_presence_of(:role).
				on(:create) }
		end
		
		context "first_name should be present" do
			it { should validate_presence_of(:first_name) }
		end
		
		context "last_name should be present" do
			it { should validate_presence_of(:last_name) }
		end

	end

	context "ActiveRecords:" do

		context "should belong to a company" do
			it { should belong_to(:company) }
		end

	end
end
