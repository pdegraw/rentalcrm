# == Schema Information
#
# Table name: contacts
#
#  id         :integer          not null, primary key
#  user_id    :integer
#  first_name :string(64)
#  last_name  :string(64)
#  title      :string(64)
#  department :string(64)
#  source     :string(32)
#  email      :string(64)
#  alt_email  :string(64)
#  phone      :string(32)
#  mobile     :string(32)
#  fax        :string(32)
#  created_at :datetime         not null
#  updated_at :datetime         not null
#  account_id :integer
#

require 'rails_helper'

RSpec.describe Contact, type: :model do
  
    context "Model:" do
      
        context "first_name should be present" do
            it { should validate_presence_of(:first_name) }
        end
        
        context "phone or email should be present" do
            it { should validate_presence_of(:phone) || validate_presence_of(:email) }
        end
        
    end
    
    context "ActiveRecords:" do

		context "should belong to a user" do
			it { should belong_to(:user) }
		end

	end
  
end
