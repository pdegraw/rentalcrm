class CreateContacts < ActiveRecord::Migration
  def change
    create_table :contacts do |t|
      t.references :user, index: true, foreign_key: true
      t.integer :assigned_to
      t.string :first_name, limit: 64
      t.string :last_name, limit: 64
      t.string :title, limit: 64
      t.string :department, limit: 64
      t.string :source, limit: 32
      t.string :email, limit: 64
      t.string :alt_email, limit: 64
      t.string :phone, limit: 32
      t.string :mobile, limit: 32
      t.string :fax, limit: 32

      t.timestamps null: false
    end
  end
end
