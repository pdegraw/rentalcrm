class CreateCompanies < ActiveRecord::Migration
  def change
    create_table :companies do |t|
      t.string :name, limit: 64

      t.timestamps null: false
    end
  end
end
