class CreateJobs < ActiveRecord::Migration
  def change
    create_table :jobs do |t|
      t.references :user, index: true, foreign_key: true
      t.references :contact, index: true, foreign_key: true
      t.string :title, limit: 64, null: false
      t.string :job_number, limit: 12
      t.string :invoice_number, limit: 12
      t.string :invoiced, limit: 12
      t.string :stage, limit: 23, default: "Inquiry" # %w(Inquiry Tentative Sold Cancelled)
      t.datetime :starts_at
      t.datetime :ends_at
      t.datetime :loads_in_at
      t.datetime :loads_out_at
      t.string :confirmed_load_times, limit: 12

      t.timestamps null: false
    end
  end
end
