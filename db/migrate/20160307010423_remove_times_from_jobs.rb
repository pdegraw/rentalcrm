class RemoveTimesFromJobs < ActiveRecord::Migration
  def change
    remove_column :jobs, :starts_on, :string
    remove_column :jobs, :ends_on, :string
  end
end
