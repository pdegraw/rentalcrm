class RemoveAssignedToFromContacts < ActiveRecord::Migration
  def change
    remove_column :contacts, :assigned_to, :integer
  end
end
