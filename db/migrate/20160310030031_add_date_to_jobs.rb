class AddDateToJobs < ActiveRecord::Migration
  def change
    add_column :jobs, :starts_on, :date
    add_column :jobs, :ends_on, :date
    add_column :jobs, :loads_in_on, :date
    add_column :jobs, :loads_out_on, :date
  end
end
