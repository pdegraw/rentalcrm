class RemoveLoadTimesFromJobs < ActiveRecord::Migration
  def change
    remove_column :jobs, :loads_in_at, :string
    remove_column :jobs, :loads_out_at, :string
  end
end
