class AddDatesToJobs < ActiveRecord::Migration
  def change
    add_column :jobs, :starts_on, :string, limit: 64
    add_column :jobs, :ends_on, :string, limit: 64
  end
end
