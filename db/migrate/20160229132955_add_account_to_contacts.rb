class AddAccountToContacts < ActiveRecord::Migration
  def change
    add_reference :contacts, :account, index: true, foreign_key: true
  end
end
