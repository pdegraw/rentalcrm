class CreateAccounts < ActiveRecord::Migration
  def change
    create_table :accounts do |t|
      t.references :user, index: true, foreign_key: true
      t.string :name, limit: 64, null: false
      t.string :website, limit: 64
      t.string :phone, limit: 32, null: false
      t.string :fax, limit: 32
      t.string :email, limit: 32

      t.timestamps null: false
    end
  end
end
