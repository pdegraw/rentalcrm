class UpdateDatesOnJobs < ActiveRecord::Migration
  def up
    change_column :jobs, :starts_at, :time
    change_column :jobs, :ends_at, :time
  end

  def down
    change_column :jobs, :starts_at, :datetime
    change_column :jobs, :ends_at, :datetime
  end
  
end
