class AddLoadTimesToJobs < ActiveRecord::Migration
  def change
    add_column :jobs, :loads_in_at, :datetime
    add_column :jobs, :loads_out_at, :datetime
  end
end
