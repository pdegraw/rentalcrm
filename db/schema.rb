# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20160310030031) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "accounts", force: :cascade do |t|
    t.integer  "user_id"
    t.string   "name",       limit: 64, null: false
    t.string   "website",    limit: 64
    t.string   "phone",      limit: 32, null: false
    t.string   "fax",        limit: 32
    t.string   "email",      limit: 32
    t.datetime "created_at",            null: false
    t.datetime "updated_at",            null: false
    t.integer  "contact_id"
  end

  add_index "accounts", ["contact_id"], name: "index_accounts_on_contact_id", using: :btree
  add_index "accounts", ["user_id"], name: "index_accounts_on_user_id", using: :btree

  create_table "companies", force: :cascade do |t|
    t.string   "name",       limit: 64
    t.datetime "created_at",            null: false
    t.datetime "updated_at",            null: false
  end

  create_table "contacts", force: :cascade do |t|
    t.integer  "user_id"
    t.string   "first_name", limit: 64
    t.string   "last_name",  limit: 64
    t.string   "title",      limit: 64
    t.string   "department", limit: 64
    t.string   "source",     limit: 32
    t.string   "email",      limit: 64
    t.string   "alt_email",  limit: 64
    t.string   "phone",      limit: 32
    t.string   "mobile",     limit: 32
    t.string   "fax",        limit: 32
    t.datetime "created_at",            null: false
    t.datetime "updated_at",            null: false
    t.integer  "account_id"
  end

  add_index "contacts", ["account_id"], name: "index_contacts_on_account_id", using: :btree
  add_index "contacts", ["user_id"], name: "index_contacts_on_user_id", using: :btree

  create_table "jobs", force: :cascade do |t|
    t.integer  "user_id"
    t.integer  "contact_id"
    t.string   "title",                limit: 64,                     null: false
    t.string   "job_number",           limit: 12
    t.string   "invoice_number",       limit: 12
    t.string   "invoiced",             limit: 12
    t.string   "stage",                limit: 23, default: "Inquiry"
    t.time     "starts_at"
    t.time     "ends_at"
    t.string   "confirmed_load_times", limit: 12
    t.datetime "created_at",                                          null: false
    t.datetime "updated_at",                                          null: false
    t.datetime "loads_in_at"
    t.datetime "loads_out_at"
    t.date     "starts_on"
    t.date     "ends_on"
    t.date     "loads_in_on"
    t.date     "loads_out_on"
  end

  add_index "jobs", ["contact_id"], name: "index_jobs_on_contact_id", using: :btree
  add_index "jobs", ["user_id"], name: "index_jobs_on_user_id", using: :btree

  create_table "users", force: :cascade do |t|
    t.string   "email",                             default: "", null: false
    t.string   "encrypted_password",                default: "", null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",                     default: 0,  null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.inet     "current_sign_in_ip"
    t.inet     "last_sign_in_ip"
    t.datetime "created_at",                                     null: false
    t.datetime "updated_at",                                     null: false
    t.integer  "role"
    t.integer  "company_id"
    t.string   "first_name",             limit: 64
    t.string   "last_name",              limit: 64
  end

  add_index "users", ["company_id"], name: "index_users_on_company_id", using: :btree
  add_index "users", ["email"], name: "index_users_on_email", unique: true, using: :btree
  add_index "users", ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true, using: :btree

  add_foreign_key "accounts", "contacts"
  add_foreign_key "accounts", "users"
  add_foreign_key "contacts", "accounts"
  add_foreign_key "contacts", "users"
  add_foreign_key "jobs", "contacts"
  add_foreign_key "jobs", "users"
  add_foreign_key "users", "companies"
end
