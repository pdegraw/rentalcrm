Rails.application.routes.draw do
  resources :jobs
  # The priority is based upon order of creation: first created -> highest priority.
  # See how all your routes lay out with "rake routes".

  root 'home#index'
  get 'home/get_events'
  
  # For use with adding company field and Recaptcha
  devise_for :users, :controllers => { :registrations => "registrations" }
  # devise_for :users
  resources :users, :companies, :contacts, :jobs
  
  resources :accounts do
    resources :countacts
  end

  # Example resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end
end
