json.array!(@jobs) do |job|
  json.extract! job, :id, :user_id, :contact_id, :title, :job_number, :invoice_number, :invoiced, :stage, :starts_at, :ends_at, :loads_in_at, :loads_out_at, :confirmed_load_times
  json.url job_url(job, format: :json)
end
