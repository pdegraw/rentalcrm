json.extract! @contact, :id, :user_id, :assigned_to, :first_name, :last_name, :title, :department, :source, :email, :alt_email, :phone, :mobile, :fax, :created_at, :updated_at
