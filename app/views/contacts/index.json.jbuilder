json.array!(@contacts) do |contact|
  json.extract! contact, :id, :user_id, :assigned_to, :first_name, :last_name, :title, :department, :source, :email, :alt_email, :phone, :mobile, :fax
  json.url contact_url(contact, format: :json)
end
