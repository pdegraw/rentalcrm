class ContactPolicy
  
  attr_reader :current_user, :model
  
  def initialize(current_user, model)
    raise Pundit::NotAuthorizedError, "Must be signed in." unless current_user
    @current_user = current_user
    @contact = model
  end
  
  def index?
    @current_user.user?
  end
  
  def show?
    @current_user.user?
  end
  
  def update?
    @current_user.user?
  end
  
  def destroy?
    @current_user.user?
  end
  
end