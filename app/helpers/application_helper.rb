module ApplicationHelper
    
    def link_to_in_li(body, url, html_options = {})
	  active = "active" if current_page?(url)
	  content_tag :li, class: active do
	    link_to body, url, html_options
	  end
	end
    
    def total_count(items)
        "#{items.count} #{controller_name}"
    end
    
    def icon(type, icon)
        if type == "glyphicon"
            self.raw("<span class='glyphicon glyphicon-#{icon} aria-hidden='true'></span>")
        elsif type == 'fa'
            self.raw("<span class='glyphicon glyphicon-#{icon} aria-hidden='true'></span>")
            self.raw("<i class='fa fa-#{icon}'></i>")
        else
            self.raw("missing icon")
        end
    end
    
    # New Entity Path
    def new_entity_path
      self.send("new_#{controller_name.singularize}_path")
    end
    
    # Phone Numbers
    def phone(number)
        number_to_phone(number, area_code: true)
    end
end
