# == Schema Information
#
# Table name: accounts
#
#  id         :integer          not null, primary key
#  user_id    :integer
#  name       :string(64)       not null
#  website    :string(64)
#  phone      :string(32)       not null
#  fax        :string(32)
#  email      :string(32)
#  created_at :datetime         not null
#  updated_at :datetime         not null
#  contact_id :integer
#

class Account < ActiveRecord::Base
    
    default_scope { order("name ASC") }
    
    belongs_to :user
    has_many :contacts
    # has_many :jobs, through: :contacts
    
    accepts_nested_attributes_for :contacts
    
    validates :name, presence: true
    
end
