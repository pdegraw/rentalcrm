# == Schema Information
#
# Table name: jobs
#
#  id                   :integer          not null, primary key
#  user_id              :integer
#  contact_id           :integer
#  title                :string(64)       not null
#  job_number           :string(12)
#  invoice_number       :string(12)
#  invoiced             :string(12)
#  stage                :string(23)       default("Inquiry")
#  starts_at            :time
#  ends_at              :time
#  confirmed_load_times :string(12)
#  created_at           :datetime         not null
#  updated_at           :datetime         not null
#  loads_in_at          :datetime
#  loads_out_at         :datetime
#  starts_on            :date
#  ends_on              :date
#  loads_in_on          :date
#  loads_out_on         :date
#

class Job < ActiveRecord::Base
  belongs_to :user
  belongs_to :contact
  
  validates :title, presence: true
  validates :contact_id, presence: true
  validates :starts_at, presence: true
  validates :ends_at, presence: true
  
  def self.filtered
		all
	end
  
  def self.active
		where('starts_on >= ?', Date.today)
	end
	
	def self.to_be_invoiced
		where('ends_on < ?', Date.today).where('invoice_number = ?', "").where('invoiced = ?', "No") || where('ends_on < ?', Date.today).where('invoice_number = ?', "").where(:invoiced  => nil)
	end
	
# 	def self.closed
# 		where(arel_table[:ends_at].lt(Date.today)).where(invoiced: "Yes").where(arel_table[:invoice_number].not_eq(nil))
# 	end
	
	def self.closed
    where('ends_on < ?', Date.today).where('invoice_number <> ?', "").where(invoiced: "Yes")
  end
  
  def self.stage(stage)
    where('starts_on >= ?', Date.today).where(stage: stage)
  end
  
  
  # Testing
	
	def self.hired_since(date)
    where('start_date >= ?', date)
  end

	scope :by_period, -> starts_at, ends_at { where("starts_on = ? AND ends_on = ?", starts_at, ends_at) }
	
end
