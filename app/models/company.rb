# == Schema Information
#
# Table name: companies
#
#  id         :integer          not null, primary key
#  name       :string(64)
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

class Company < ActiveRecord::Base
    has_many :users
    
    validates :name, presence: true
end
