# == Schema Information
#
# Table name: contacts
#
#  id         :integer          not null, primary key
#  user_id    :integer
#  first_name :string(64)
#  last_name  :string(64)
#  title      :string(64)
#  department :string(64)
#  source     :string(32)
#  email      :string(64)
#  alt_email  :string(64)
#  phone      :string(32)
#  mobile     :string(32)
#  fax        :string(32)
#  created_at :datetime         not null
#  updated_at :datetime         not null
#  account_id :integer
#

class Contact < ActiveRecord::Base
  
  default_scope { order("last_name ASC") }
  
  belongs_to :user
  belongs_to :account
  has_many :jobs
  
  accepts_nested_attributes_for :account
  
  validates :first_name, presence: true
  
  validates :phone, presence: true, :unless => :email?
  validates :email, presence: true, :unless => :phone?
  
  def full_name
    "#{@contact.first_name} #{@contact.last_name}"
  end
  
  def full_name_association
    "#{first_name} #{last_name}"
  end
  
  def full_name_and_account_association
    if !account_id.nil?
      account = Account.find(account_id)
      "#{first_name} #{last_name}, #{account.name}"
    else
      "#{first_name} #{last_name}"
    end
  end
    
end
