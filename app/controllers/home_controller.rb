class HomeController < ApplicationController
    def index
        @jobs = Job.all.order(:starts_on).order(:starts_at)
        @jobs_this_week = Job.where("starts_on <= ?", Date.today.end_of_week).order(:starts_on).order(:starts_at)
    end
    
    respond_to :json
    def get_events
        @job = current_user.jobs.order(:starts_on).order(:starts_at)
        events = []
        @job.each do |job|
            events << {
                :id => job.id,
                :title => "#{job.try(:title)}",
                :start => "#{job.starts_on.try(:strftime, "%F")}T#{job.starts_at.try(:strftime, "%H:%M:%S%z")}",
                :end => "#{job.ends_on.try(:strftime, "%F")}T#{job.ends_at.try(:strftime, "%H:%M:%S%z")}",
                :url => "jobs/#{job.id}",
                :details => "#{job.title} #{job.starts_at.try(:strftime, "@ %-l:%M %P")}#{job.ends_at.try(:strftime, "-%-l:%M %P")}"
            }
        end
        render :text => events.to_json
    end
end