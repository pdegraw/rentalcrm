# == Schema Information
#
# Table name: contacts
#
#  id         :integer          not null, primary key
#  user_id    :integer
#  first_name :string(64)
#  last_name  :string(64)
#  title      :string(64)
#  department :string(64)
#  source     :string(32)
#  email      :string(64)
#  alt_email  :string(64)
#  phone      :string(32)
#  mobile     :string(32)
#  fax        :string(32)
#  created_at :datetime         not null
#  updated_at :datetime         not null
#  account_id :integer
#

class ContactsController < ApplicationController
  before_filter :authenticate_user!
  before_action :set_contact, only: [:show, :edit, :update, :destroy]

  # GET /contacts
  # GET /contacts.json
  def index
    # @contacts = policy_scope(Contact)
    # @q = policy_scope(Contact).includes(:account).ransack(params[:q])
    @q = Contact.includes(:account).ransack(params[:q])
    @contacts = @q.result.paginate(:page => params[:page], :per_page => 10)
    if @contacts.count > 0 && @contacts.empty?
      redirect_to new_contact_path, notice: "no results for #{@q.first_name_or_last_name_or_account_name_cont}"
    elsif @contacts.count == 0
      redirect_to new_contact_path, notice: "There are no contacts yet, go ahead and create one."
    end
    @new_contact = Contact.new
    
    @recent = Contact.order(created_at: :desc).limit(5)
    # authorize Contact
    # raise Pundit::NotAuthorized unless current_user.role? :staff
  end

  # GET /contacts/1
  # GET /contacts/1.json
  def show
    @job_years = @contact.jobs.order("starts_on DESC").group_by { |jobs| jobs.starts_on.beginning_of_year }
    # authorize @contact
  end

  # GET /contacts/new
  def new
    @contact = Contact.new
    # authorize @contact
  end

  # GET /contacts/1/edit
  def edit
    # authorize @contact
  end

  # POST /contacts
  # POST /contacts.json
  def create
    @contact = Contact.new(contact_params)
    
    @contact.user_id = current_user.id
    
    # authorize @contact

    respond_to do |format|
      if @contact.save
        format.html { redirect_to @contact, notice: 'Contact was successfully created.' }
        format.json { render :show, status: :created, location: @contact }
      else
        format.html { render :new }
        format.json { render json: @contact.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /contacts/1
  # PATCH/PUT /contacts/1.json
  def update
    authorize @contact
    respond_to do |format|
      if @contact.update(contact_params)
        format.html { redirect_to @contact, notice: 'Contact was successfully updated.' }
        format.json { render :show, status: :ok, location: @contact }
      else
        format.html { render :edit }
        format.json { render json: @contact.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /contacts/1
  # DELETE /contacts/1.json
  def destroy
    authorize @contact
    @contact.destroy
    respond_to do |format|
      format.html { redirect_to contacts_url, notice: 'Contact was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_contact
      @contact = Contact.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def contact_params
      params.require(:contact).permit(:user_id, :assigned_to, :first_name, :last_name, :title, :department, :source, :email, :alt_email, :phone, :mobile, :fax, :account_id)
    end
end
