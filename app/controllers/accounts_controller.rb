# == Schema Information
#
# Table name: accounts
#
#  id         :integer          not null, primary key
#  user_id    :integer
#  name       :string(64)       not null
#  website    :string(64)
#  phone      :string(32)       not null
#  fax        :string(32)
#  email      :string(32)
#  created_at :datetime         not null
#  updated_at :datetime         not null
#  contact_id :integer
#

class AccountsController < ApplicationController
  before_action :set_account, only: [:show, :edit, :update, :destroy]

  # GET /accounts
  # GET /accounts.json
  def index
    @q = Account.ransack(params[:q])
    @accounts = @q.result.paginate(:page => params[:page], :per_page => 10)
    @new_account = Account.new
    
    @recent = Account.order('created_at DESC').limit(5)
  end

  # GET /accounts/1
  # GET /accounts/1.json
  def show
    @contacts = @account.contacts.order("last_name DESC")
    # @jobs = @account.contacts.jobs
    # @job_years = @account.contacts.jobs.order("starts_on DESC").group_by { |jobs| jobs.starts_on.beginning_of_year }
    @contact = Contact.new
  end

  # GET /accounts/new
  def new
    @account = Account.new
  end

  # GET /accounts/1/edit
  def edit
  end

  # POST /accounts
  # POST /accounts.json
  def create
    @account = Account.new(account_params)
    
    @account.user_id = current_user.id

    respond_to do |format|
      if @account.save
        format.html { redirect_to @account, notice: 'Account was successfully created.' }
        format.json { render :show, status: :created, location: @account }
      else
        format.html { render :new }
        format.json { render json: @account.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /accounts/1
  # PATCH/PUT /accounts/1.json
  def update
    respond_to do |format|
      if @account.update(account_params)
        format.html { redirect_to @account, notice: 'Account was successfully updated.' }
        format.json { render :show, status: :ok, location: @account }
      else
        format.html { render :edit }
        format.json { render json: @account.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /accounts/1
  # DELETE /accounts/1.json
  def destroy
    @account.destroy
    respond_to do |format|
      format.html { redirect_to accounts_url, notice: 'Account was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_account
      @account = Account.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def account_params
      params.require(:account).permit(:user_id, :name, :website, :phone, :fax, :email)
    end
end
