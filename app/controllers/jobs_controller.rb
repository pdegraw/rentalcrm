# == Schema Information
#
# Table name: jobs
#
#  id                   :integer          not null, primary key
#  user_id              :integer
#  contact_id           :integer
#  title                :string(64)       not null
#  job_number           :string(12)
#  invoice_number       :string(12)
#  invoiced             :string(12)
#  stage                :string(23)       default("Inquiry")
#  starts_at            :time
#  ends_at              :time
#  confirmed_load_times :string(12)
#  created_at           :datetime         not null
#  updated_at           :datetime         not null
#  loads_in_at          :datetime
#  loads_out_at         :datetime
#  starts_on            :date
#  ends_on              :date
#  loads_in_on          :date
#  loads_out_on         :date
#

class JobsController < ApplicationController
  before_action :set_job, only: [:show, :edit, :update, :destroy]
  
  has_scope :filtered, :type => :boolean
  has_scope :active, :type => :boolean
  has_scope :closed, :type => :boolean
  has_scope :to_be_invoiced, :type => :boolean
  
  has_scope :stage

  # GET /jobs
  # GET /jobs.json
  def index
    if params[:q]
      @q = Job.ransack(params[:q])
      @jobs = @q.result.paginate(:page => params[:page], :per_page => 6)
    else
      @q = Job.ransack(params[:q])
      @jobs = apply_scopes(Job).order("starts_on ASC").paginate(:page => params[:page], :per_page => 6)
		end
		
		@job_weeks = @jobs.order(:starts_on).group_by { |jobs| jobs.starts_on.beginning_of_week }
		@new_job = Job.new
		
		@count_jobs = Job.all.count
    @count_filtered = Job.filtered.count unless Job.filtered.count == 0
    @count_active = Job.active.count unless Job.active.count == 0
    @count_closed = Job.closed.count unless Job.closed.count == 0
    @count_to_be_invoiced = Job.to_be_invoiced.count unless Job.to_be_invoiced.count == 0
    
    # Counts for statuses
    @count_inquiry = Job.stage("Inquiry").count
    @count_tentative = Job.stage("Tentative").count
    @count_sold = Job.stage("Sold").count
    @count_cancelled = Job.stage("Cancelled").count
  end

  # GET /jobs/1
  # GET /jobs/1.json
  def show
  end

  # GET /jobs/new
  def new
    @job = Job.new
  end

  # GET /jobs/1/edit
  def edit
  end

  # POST /jobs
  # POST /jobs.json
  def create
    @job = Job.new(job_params)
    
    @job.user_id = current_user.id

    respond_to do |format|
      if @job.save
        format.html { redirect_to @job, notice: 'Job was successfully created.' }
        format.json { render :show, status: :created, location: @job }
      else
        format.html { render :new }
        format.json { render json: @job.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /jobs/1
  # PATCH/PUT /jobs/1.json
  def update
    respond_to do |format|
      if @job.update(job_params)
        format.html { redirect_to @job, notice: 'Job was successfully updated.' }
        format.json { render :show, status: :ok, location: @job }
      else
        format.html { render :edit }
        format.json { render json: @job.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /jobs/1
  # DELETE /jobs/1.json
  def destroy
    @job.destroy
    respond_to do |format|
      format.html { redirect_to jobs_url, notice: 'Job was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_job
      @job = Job.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def job_params
      params.require(:job).permit(:user_id, :contact_id, :title, :job_number, :invoice_number, :invoiced, :stage, :starts_on, :starts_at, :ends_on, :ends_at, :loads_in_on, :loads_in_at, :loads_out_on, :loads_out_at, :confirmed_load_times)
    end
end
