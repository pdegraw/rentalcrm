# == Schema Information
#
# Table name: users
#
#  id                     :integer          not null, primary key
#  email                  :string           default(""), not null
#  encrypted_password     :string           default(""), not null
#  reset_password_token   :string
#  reset_password_sent_at :datetime
#  remember_created_at    :datetime
#  sign_in_count          :integer          default(0), not null
#  current_sign_in_at     :datetime
#  last_sign_in_at        :datetime
#  current_sign_in_ip     :inet
#  last_sign_in_ip        :inet
#  created_at             :datetime         not null
#  updated_at             :datetime         not null
#  role                   :integer
#  company_id             :integer
#  first_name             :string(64)
#  last_name              :string(64)
#

class UsersController < ApplicationController
  before_filter :authenticate_user!
  after_action :verify_authorized
  
  def index
    @q_user = User.search(params[:q])
    @users = @q_user.result.paginate(:page => params[:page], :per_page => 10)
    authorize User
  end
  
  def show
    # @user = User.find(params[:id])
    @user = current_user.id
    # @techniques = Technique.where("user_id =?", @user.id)
    authorize @user
  end
  
  def destroy
    user = User.find(params[:id])
    authorize user
    user.destroy
    redirect_to users_path, :notice => "User was successfully deleted."
  end
  
  def update
    @user = User.find(params[:id])
    authorize @user
    
    if @user.update_attributes(secure_params)
      redirect_to users_path, notice: "User Updated"
    else
      redirect_to users_path, notice: "Unable to update user."
    end
  end
  
  private

    def secure_params
      params.require(:user).permit(:role)
    end
  
end
